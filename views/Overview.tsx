import React from 'react'
import { NavigationWrapper } from './NavigationWrapper'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackParamList } from '../components/Navigation'
import { Route } from '@react-navigation/native'
import { LargeFigure } from '../components/LargeFigure'
import { View, StyleSheet } from 'react-native'

export const Overview = ({
  navigation,
  route,
}: {
  navigation: StackNavigationProp<
    RootStackParamList,
    'Overview' | 'Entries' | 'Statistics'
  >
  route: Route<'Overview' | 'Entries' | 'Statistics'>
}) => (
  <NavigationWrapper navigation={navigation} route={route}>
    <View style={styles.figures}>
      <LargeFigure figcaption="Today" figure={7} />
      <LargeFigure figcaption="Week" figure={37.5} />
    </View>
  </NavigationWrapper>
)

const styles = StyleSheet.create({
  figures: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: '10%',
  },
})
