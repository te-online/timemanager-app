import React, { ReactElement } from 'react'
import { View, StyleSheet, SafeAreaView } from 'react-native'
import { Navigation, RootStackParamList } from '../components/Navigation'
import { StackNavigationProp } from '@react-navigation/stack'
import { Route } from '@react-navigation/native'
import Constants from 'expo-constants'

export const NavigationWrapper = ({
  children,
  navigation,
  route,
}: {
  children: ReactElement
  navigation: StackNavigationProp<
    RootStackParamList,
    'Overview' | 'Entries' | 'Statistics'
  >
  route: Route<'Overview' | 'Entries' | 'Statistics'>
}) => (
  <SafeAreaView style={styles.container}>
    {children}
    <Navigation navigation={navigation} route={route} />
  </SafeAreaView>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: Constants.statusBarHeight,
  },
})
