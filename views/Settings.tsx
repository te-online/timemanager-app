import React from 'react'
import { NavigationWrapper } from './NavigationWrapper'
import { Text } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackParamList } from '../components/Navigation'
import { Route } from '@react-navigation/native'

export const Settings = ({
  navigation,
  route,
}: {
  navigation: StackNavigationProp<
    RootStackParamList,
    'Overview' | 'Entries' | 'Statistics'
  >
  route: Route<'Overview' | 'Entries' | 'Statistics'>
}) => (
  <NavigationWrapper navigation={navigation} route={route}>
    <Text>Settings Content</Text>
  </NavigationWrapper>
)
