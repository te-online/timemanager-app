import React from 'react'
import { NavigationWrapper } from './NavigationWrapper'
import { FlatList, Text, StyleSheet } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackParamList } from '../components/Navigation'
import { Route } from '@react-navigation/native'
import { ListItem } from 'react-native-elements'

const styles = StyleSheet.create({
  listHeader: {
    color: '#95989A',
    textTransform: 'uppercase',
    letterSpacing: 1,
    fontSize: 10,
    paddingTop: 10,
    paddingLeft: 16,
  },
})

const DATA = [
  {
    name: 'Acme Corp.',
    uuid: 'abcdefg',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
  {
    name: 'Acme Corp.',
    uuid: 'bcdefg',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
  {
    name: 'Acme Corp.',
    uuid: 'cdefg',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
  {
    name: 'Acme Corp.',
    uuid: 'defg',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
  {
    name: 'Acme Corp.',
    uuid: 'efg',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
  {
    name: 'Acme Corp.',
    uuid: 'fg',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
  {
    name: 'Acme Corp.',
    uuid: 'g',
    stats: {
      projects: 10,
      hours: 19,
      since: 2015,
    },
  },
]

const Item = ({
  name,
  stats,
  onPress,
}: {
  name: string
  stats: {
    projects: number
    hours: number
    since: number
  }
  onPress: () => void
}) => (
  <ListItem
    title={name}
    subtitle={`${stats.projects} projects – ${stats.hours} hrs. – since ${stats.since}`}
    bottomDivider
    chevron
    onPress={onPress}
  />
)

const ListHeader = <Text style={styles.listHeader}>Clients</Text>

export const Entries = ({
  navigation,
  route,
}: {
  navigation: StackNavigationProp<
    RootStackParamList,
    'Overview' | 'Entries' | 'Statistics'
  >
  route: Route<'Overview' | 'Entries' | 'Statistics'>
}) => (
  <NavigationWrapper navigation={navigation} route={route}>
    <FlatList
      data={DATA}
      renderItem={({ item }) => (
        <Item
          name={item.name}
          stats={item.stats}
          // selected={() => {}}
          // onSelect={() => {}}
          onPress={() => {
            navigation.navigate('Statistics')
          }}
        />
      )}
      keyExtractor={(item) => item.uuid}
      ListHeaderComponent={ListHeader}
      // extraData={selected}
    />
  </NavigationWrapper>
)
