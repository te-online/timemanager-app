import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

export const LargeFigure = ({
  figcaption,
  figure,
}: {
  figcaption: string
  figure: number
}) => (
  <View style={styles.container}>
    <Text style={styles.figcaption}>{figcaption}</Text>
    <Text style={styles.figure}>{figure} hrs.</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  figcaption: {
    fontSize: 14,
    color: '#95989A',
    textTransform: 'uppercase',
  },
  figure: {
    fontSize: 42,
    color: '#7D7D7D',
  },
})
