import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { BorderlessButton } from 'react-native-gesture-handler'
import { StackNavigationProp } from '@react-navigation/stack'
import { Route } from '@react-navigation/native'

export const Navigation = ({
  navigation,
  route,
}: {
  navigation: StackNavigationProp<
    RootStackParamList,
    'Overview' | 'Entries' | 'Statistics'
  >
  route: Route<'Overview' | 'Entries' | 'Statistics'>
}) => (
  <View style={styles.container}>
    <View style={styles.entry}>
      <BorderlessButton onPress={() => navigation.navigate('Overview')}>
        <Image
          source={
            route.name === 'Overview'
              ? require('../assets/overview-icon-active.png')
              : require('../assets/overview-icon.png')
          }
          accessibilityLabel="Overview"
        ></Image>
      </BorderlessButton>
    </View>
    <View style={styles.entry}>
      <BorderlessButton onPress={() => navigation.navigate('Entries')}>
        <Image
          source={
            route.name === 'Entries'
              ? require('../assets/entries-icon-active.png')
              : require('../assets/entries-icon.png')
          }
          accessibilityLabel="Entries"
        ></Image>
      </BorderlessButton>
    </View>
    <View style={styles.entry}>
      <BorderlessButton onPress={() => navigation.navigate('Statistics')}>
        <Image
          source={
            route.name === 'Statistics'
              ? require('../assets/statistics-icon-active.png')
              : require('../assets/statistics-icon.png')
          }
          accessibilityLabel="Statistics"
        ></Image>
      </BorderlessButton>
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#EBEBEB',
    alignItems: 'center',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
  },
  entry: {
    flex: 1,
    height: 86,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export type RootStackParamList = {
  Overview: undefined
  Entries: { userId: string } | undefined
  Statistics: { sort: 'latest' | 'top' } | undefined
}
