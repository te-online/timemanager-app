# TimeManager App

A mobile app for local time tracking on a device or as a sync companion to the Nextcloud [TimeManager app](https://github.com/te-online/timemanager).

## Features
@TODO

## Development
This app uses React-Native, Expo and Typescript for a single, typed codebase that works cross-platform.

Before building or development, dependencies need to be installed once, by running `npm install`. The Expo CLI needs to be installed globally, however Expo will install it automatically when you start if for the first time.

Run the Expo server by executing `npm start`.

A virtual device simulator or a physical device with the “Expo” app (from the AppStore/Play Store) installed.

## Changelog
@TODO