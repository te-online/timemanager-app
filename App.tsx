import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { Overview } from './views/Overview'
import { Entries } from './views/Entries'
import { Statistics } from './views/Statistics'

const Stack = createStackNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Overview"
          component={Overview}
          options={{ header: () => null }}
        />
        <Stack.Screen
          name="Entries"
          component={Entries}
          options={{ header: () => null }}
        />
        <Stack.Screen
          name="Statistics"
          component={Statistics}
          options={{ header: () => null }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
